package poc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Hello world!
 *
 */
public class App 
{
    private static final Logger LOG = LogManager.getLogger("App");
    public static void main( String[] args )
    {
        LOG.info("Start");
        System.out.println( "Hello World!" );
        LOG.info("End");
    }
}
